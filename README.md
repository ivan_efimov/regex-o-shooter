## Regex-o-shooter

### Русский
Простой космический шутер, но управление --- посредством регулярных выражений.

Приложение имеет только браузерную часть и может быть скачано и локально запущено в браузере.
Также приложение размещено на [heroku](https://regex-o-shooter.herokuapp.com/).

Правила игры размещены на начальном экране игры, под кнопкой **Помощь**.

Информацию о лицензировании смотрите в файлах `LICENSE` и `gpl-3.0.txt`.

## English
Simple space shooter game, but control is by regular expressions.

This application is browser-only, so it can be simply downloaded and opened in a browser.
It is also deployed on [heroku](https://regex-o-shooter.herokuapp.com/).

See game rules in the game start screen beyond **Help** button.

For license information look at `LICENSE` and `gpl-3.0.txt` files.
