// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

let resourceManager = null;

(async () => {
    resourceManager = await loadResources();
    main()
})();

function main() {
    let loadingScreen = document.getElementById("loading_screen");
    let difficultyScreen = document.getElementById("difficulty_screen");
    let easyButton = document.getElementById("easy_button");
    let mediumButton = document.getElementById("medium_button");
    let hardButton = document.getElementById("hard_button");
    let extremeButton = document.getElementById("extreme_button");
    loadingScreen.classList.add("hidden");
    difficultyScreen.classList.remove("hidden");
    easyButton.onclick = function () { difficultyScreen.classList.add("hidden"); game("easy") };
    mediumButton.onclick = function () { difficultyScreen.classList.add("hidden"); game("medium") };
    hardButton.onclick = function () { difficultyScreen.classList.add("hidden"); game("hard") };
    extremeButton.onclick = function () { difficultyScreen.classList.add("hidden"); game("extreme") };
}

function game(difficulty) {
    let gameOverScreen = document.getElementById("game_over_screen");

    let strikeRequired = false;
    let regexField = document.getElementById("regex_field");
    regexField.addEventListener("keydown", function (ev) {
        if (ev.key === "Enter") {
            strikeRequired = true;
        }
    });

    let health = document.getElementById("health");
    let scores = document.getElementById("scores");

    let gfLeft = -50;
    let gfRight = 50;
    let gfBottom = 0;
    let gfTop = 100;

    let gameRenderer = new GameRenderer(gfLeft, gfRight, gfTop, gfBottom, GameState.BaseHeightCoefficient, window.innerWidth, window.innerHeight, resourceManager);
    window.addEventListener("resize", function () {
        gameRenderer.onWindowResize(window.innerWidth, window.innerHeight);
    });

    let gameState = new GameState(gameRenderer);
    regexField.focus();

    function render() {
        if (gameState.health <= 0) {
            let scores_total = document.getElementById("score_total");
            scores_total.innerText = "Result: " + gameState.scores.toString();
            gameOverScreen.classList.remove("hidden");
        } else {
            setTimeout(function () {

                requestAnimationFrame(render);

            }, 1000 / 60);


            gameState.update(prepareRegexp(regexField.value), strikeRequired);
            health.innerText = gameState.health.toString();
            scores.innerText = gameState.scores.toString();
            if (health > GameState.InitialHealth * 0.2) {
                health.classList.remove("low_health");
            } else {
                health.classList.add("low_health");
            }
            if (strikeRequired) {
                regexField.value = '';
            }
            strikeRequired = false;
        }
    }


    gameState.reset(gfLeft, gfRight, gfTop, gfBottom, difficulty);
    health.innerText = gameState.health.toString();
    scores.innerText = gameState.scores.toString();
    health.classList.remove("low_health");

    render();

    function prepareRegexp(text) {
        if (text.length === 0) {
            return new RegExp("^$");
        }
        try {
            return new RegExp(text);
        } catch (e) {
            return new RegExp("^$");
        }
    }
}
