// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

class UnitStats {
    constructor(velocity, radius) {
        this.velocity = velocity;
        this.radius = radius;
    }
}

class Unit {
    reachedTargetFunc = function (pos) {
        return false;
    }
    static AimedBgColor = "rgba(200, 100, 100, 255)";
    static NotAimedBgColor = "rgba(200, 200, 200, 255)";
    static NameColor = "rgba(10, 10, 10, 255)";

    constructor(position, stats, patternName, name, reachedTargetFunc) {
        this.position = position;
        this.stats = stats;
        this.name = name;
        this.reachedTargetFunc = reachedTargetFunc;
        this.patternName = patternName;
        this.isAimed = false;
    }

    update(dt, gameRenderer) {
        this.position.x += dt * this.stats.velocity.x;
        this.position.y += dt * this.stats.velocity.y;
        gameRenderer.drawUnit(
            this.position,
            this.name,
            this.patternName,
            Unit.NameColor,
            this.isAimed ? Unit.AimedBgColor : Unit.NotAimedBgColor);
        return this.reachedTargetFunc(this.position)
    }
}

class Line {
    constructor(x1, y1, x2, y2) {
        this.p1 = {x: x1, y: y1};
        this.p2 = {x: x2, y: y2};
    }
}

class UnitGroup {
    constructor(gameRenderer, spawnLine, reachedTargetFunc, templateStats, patternName) {
        this.gameRenderer = gameRenderer;
        this.spawnLine = spawnLine;
        this.reachedTargetFunc = reachedTargetFunc;
        this.templateStats = templateStats;
        this.patternName = patternName;
        this.units = []
    }

    spawn(name, stats = null) {
        let pos
        if (this.spawnLine.p1 === this.spawnLine.p2) {
            pos = this.spawnLine.p1
        } else {
            let scale = Math.random();
            pos = {
                x: this.spawnLine.p1.x + scale * (this.spawnLine.p2.x - this.spawnLine.p1.x),
                y: this.spawnLine.p1.y + scale * (this.spawnLine.p2.y - this.spawnLine.p1.y)
            }
        }
        let unit = new Unit(pos, stats == null ? this.templateStats : stats, this.patternName, name, this.reachedTargetFunc);
        this.units.push(unit);
    }

    update(dt) {
        let reachedCount = 0
        for (let i = 0; i < this.units.length;) {
            if (this.units[i].update(dt, this.gameRenderer)) {
                this.units.splice(i, 1)
                reachedCount++;
            } else {
                i++;
            }
        }
        return reachedCount;
    }

    count() {
        return this.units.length;
    }

    strike() {
        let hitCount = 0
        for (let i = 0; i < this.units.length;) {
            if (this.units[i].isAimed) {
                this.gameRenderer.addStrike(this.units[i].position);
                this.gameRenderer.addCrash(this.units[i].position, this.patternName)
                this.units.splice(i, 1)
                hitCount++;
            } else {
                i++;
            }
        }
        return hitCount;
    }

    check(expression) {
        for (let i = 0; i < this.units.length; i++) {
            this.units[i].isAimed = expression.test(this.units[i].name);
        }
    }

    nameIsPresent(name) {
        for (let unit in this.units) {
            if (name === unit.name) {
                return true;
            }
        }
        return false;
    }
}
