// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

function showHowto() {
    fillHowtos();
    const howtoScreen = document.getElementById("howto_screen");
    howtoScreen.classList.remove("hidden");
}

function hideHowto() {
    const howtoScreen = document.getElementById("howto_screen");
    howtoScreen.classList.add("hidden");
}

function fillHowtos() {
    const rus = document.getElementById("russian_howto_text");
    const eng = document.getElementById("english_howto_text");
    rus.innerHTML =
        "Введите <i>регулярное выражение</i> в текстовое поле, нажмите <b>Enter</b> и все юниты, удовлетворяющие " +
        "ему, будут уничтожены. За каждую атаку Вы получаете очки пропорционально квадрату числа " +
        "уничтоженных врагов и теряете здровье пропорционально квадрату числа уничтоженных союзников. " +
        "Вы также теряете здоровье за каждого врага, достигшего вашей стороны поля. Число юнитов и " +
        "их скорость постепенно растут.";
    eng.innerHTML =
        "Type a <i>regular expression</i>, press <b>Enter</b>, and all matching units will be exterminated. For each " +
        "strike you earn scores as a square of number of killed enemies and lose health as a square " +
        "of number of killed allies. You also lose health when an enemy reaches your side of the field. " +
        "Amount of units and their speed are increasing slowly.";
}