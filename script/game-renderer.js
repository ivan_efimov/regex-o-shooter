// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

class GameRenderer {
    static FieldColor = "rgba(80, 80, 80, 255)";
    static StrikeColor = "rgba(255, 255, 0, 12)";
    static StrikeSteps = 10;
    static CrashAnimationSlowdown = 3;

    constructor(gfLeft, gfRight, gfTop, gfBottom, gfBaseHeightCoefficient, windowWidth, windowHeight, resourceManager) {
        this.wWidth = null;
        this.wHeight = null;

        this.fieldPos = null;
        this.fieldWidth = null;
        this.fieldHeight = null;
        this.basePoint = null;

        this.canvas = null;
        this.context = null;

        this.gfLeft = gfLeft;
        this.gfRight = gfRight;
        this.gfTop = gfTop;
        this.gfBottom = gfBottom;
        this.gfBaseHeightCoefficient = gfBaseHeightCoefficient;

        this.strikes = [];
        this.crashes = [];

        this.resourceManager = resourceManager;
        this.bgPattern = null;
        this.basePattern = null;
        this.allyPattern = null;
        this.enemyPattern = null;
        this.allyCrashPatterns = [];
        this.enemyCrashPatterns = [];

        this.onWindowResize(windowWidth, windowHeight);
    }

    onWindowResize(windowWidth, windowHeight) {
        this.wWidth = windowWidth;
        this.wHeight = windowHeight;

        this.fieldPos = this.xyToScreenCoords({x: this.gfLeft, y: this.gfTop});
        this.fieldWidth = this.wWidth;
        this.fieldHeight = this.wHeight * (1 - this.gfBaseHeightCoefficient);
        this.basePoint = {x: this.fieldPos.x + this.fieldWidth / 2, y: this.fieldPos.y + this.fieldHeight};

        if (!this.canvas) {
            this.canvas = document.createElement('canvas');
            document.body.appendChild(this.canvas)
            this.canvas.style.position = "absolute";
            this.canvas.style.top = "0";
            this.canvas.style.display = "block";
            this.context = this.canvas.getContext('2d');
        }

        this.canvas.width = windowWidth;
        this.canvas.height = windowHeight;
        this.canvas.style.width = windowWidth + "px";
        this.canvas.style.height = windowHeight + "px";

        this.bgPattern = this.context.createPattern(this.resourceManager.background, "repeat");
        this.basePattern = this.context.createPattern(this.resourceManager.base, "repeat");
        this.allyPattern = this.context.createPattern(this.resourceManager.ally, "repeat");
        this.enemyPattern = this.context.createPattern(this.resourceManager.enemy, "repeat");
        for (let i = 0; i < this.resourceManager.allyCrash.length; i++) {
            this.allyCrashPatterns.push(this.context.createPattern(this.resourceManager.allyCrash[i], "repeat"));
        }
        for (let i = 0; i < this.resourceManager.enemyCrash.length; i++) {
            this.enemyCrashPatterns.push(this.context.createPattern(this.resourceManager.enemyCrash[i], "repeat"));
        }
    }

    drawUnit(pos, text, patternName /* some of: ["ally", "enemy"] */, textColor, textBgColor) {
        let screenPos = this.xyToScreenCoords(pos);

        switch (patternName) {
            case "ally":
                this.context.fillStyle = this.allyPattern;
                break;
            case "enemy":
                this.context.fillStyle = this.enemyPattern;
                break;
            default:
                this.context.fillStyle = "rgba(255, 128, 128, 200)";
        }
        this.context.translate(screenPos.x - ResourceManager.UnitSize / 2, screenPos.y - ResourceManager.UnitSize / 2);
        this.context.fillRect(0, 0, ResourceManager.UnitSize, ResourceManager.UnitSize);
        this.context.setTransform(1, 0, 0, 1, 0, 0);

        let measure = this.context.measureText(text);
        this.context.fillStyle = textBgColor;
        this.context.lineWidth = 2;
        this.context.strokeStyle = "black";
        this.context.fillRect(screenPos.x - measure.width / 2 * 1.1, screenPos.y - ResourceManager.UnitSize - 26, measure.width * 1.1, 34);
        this.context.strokeRect(screenPos.x - measure.width / 2 * 1.1, screenPos.y - ResourceManager.UnitSize - 26, measure.width * 1.1, 34);

        this.context.font = "normal 30px monospace"
        this.context.textAlign = "center";
        this.context.fillStyle = textColor;
        this.context.fillText(text, screenPos.x, screenPos.y - ResourceManager.UnitSize);
    }

    addStrike(targetPos) {
        let screenPos = this.xyToScreenCoords(targetPos);
        this.strikes.push({
            x: screenPos.x,
            y: screenPos.y,
            step: GameRenderer.StrikeSteps,
        })
    }

    drawStrikes() {
        for (let i = 0; i < this.strikes.length; i++) {
            this.context.strokeStyle = GameRenderer.StrikeColor;
            this.context.lineWidth = this.strikes[i].step * 0.75;
            this.context.lineCap = 'round';
            this.context.beginPath();
            this.context.moveTo(this.basePoint.x, this.basePoint.y);
            this.context.lineTo(this.strikes[i].x, this.strikes[i].y);
            this.context.stroke();
            this.strikes[i].step--;
            if (this.strikes[i].step === 0) {
                this.strikes.splice(i, 1);
                i--;
            }
        }
    }

    addCrash(pos, patternName) {
        if (patternName !== "ally" && patternName !== "enemy") {
            return;
        }
        let screenPos = this.xyToScreenCoords(pos);
        this.crashes.push({
            x: screenPos.x,
            y: screenPos.y,
            step: (patternName === "ally" ? this.allyCrashPatterns.length : this.enemyCrashPatterns.length) * GameRenderer.CrashAnimationSlowdown,
            patternName: patternName,
        });
    }

    drawCrashes() {
        for (let i = 0; i < this.crashes.length; i++) {
            switch (this.crashes[i].patternName) {
                case "ally":
                    this.context.fillStyle = this.allyCrashPatterns[this.allyCrashPatterns.length - Math.ceil(this.crashes[i].step / GameRenderer.CrashAnimationSlowdown)];
                    break;
                case "enemy":
                    this.context.fillStyle = this.enemyCrashPatterns[this.enemyCrashPatterns.length - Math.ceil(this.crashes[i].step / GameRenderer.CrashAnimationSlowdown)];
            }
            this.context.translate(this.crashes[i].x - ResourceManager.UnitCrashSize / 2, this.crashes[i].y - ResourceManager.UnitCrashSize / 2);
            this.context.fillRect(0, 0, ResourceManager.UnitCrashSize, ResourceManager.UnitCrashSize);
            this.context.setTransform(1, 0, 0, 1, 0, 0);
            this.crashes[i].step--;
            if (this.crashes[i].step === 0) {
                this.crashes.splice(i, 1);
                i--;
            }
        }
    }

    clear() {
        this.context.clearRect(0, 0, this.wWidth, this.wHeight);
        this.context.fillStyle = this.bgPattern;
        this.context.fillRect(this.fieldPos.x, this.fieldPos.y, this.fieldWidth, this.fieldHeight);

        this.context.fillStyle = this.basePattern;
        this.context.translate(this.fieldPos.x, this.fieldPos.y + this.fieldHeight);
        this.context.fillRect(0, 0, this.fieldWidth, this.wHeight - this.fieldHeight);
        this.context.setTransform(1, 0, 0, 1, 0, 0);
    }

    xyToScreenCoords(pos) {
        return {
            x: Math.floor(this.wWidth * (pos.x - this.gfLeft) / (this.gfRight - this.gfLeft)),
            y: Math.floor(this.wHeight * (this.gfTop - pos.y) / (this.gfTop - this.gfBottom))
        };
    }
}