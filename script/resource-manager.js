// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

class ResourceManager {
    static UnitSize = 32;
    static UnitCrashSize = 64;
    constructor() {
        this.background = null;
        this.enemy = null;
        this.ally = null;
        this.base = null;
        this.allyCrash = [];
        this.enemyCrash = [];
    }
    static async loadPng(path) {
        return new Promise(resolve => {
            const img = new Image();
            img.addEventListener("load", () => {
                resolve(img);
            });
            img.src = path;
        })
    }
}

async function loadResources() {
    const rm = new ResourceManager();
    rm.background = await ResourceManager.loadPng("resources/space.png");
    rm.base = await ResourceManager.loadPng("resources/base.png");
    rm.ally = await ResourceManager.loadPng("resources/ally.png");
    rm.enemy = await ResourceManager.loadPng("resources/enemy.png");
    for (let i = 0; i < 5; i++) {
        rm.allyCrash.push(await ResourceManager.loadPng("resources/ally_crash_" + i + ".png"));
        rm.enemyCrash.push(await ResourceManager.loadPng("resources/enemy_crash_" + i + ".png"));
    }
    return rm;
}