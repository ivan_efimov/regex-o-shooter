// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

class NameProvider {
    constructor(difficulty) {
        this.basis = "abcdefghijklmnopqrstuvwxyz.-+$^?*";
        this.wordlen = 5;
    }

    getName() {
        let name = ""
        for (let i = 0; i < this.wordlen; i++) {
            name += this.basis[this.getRandomInt(0, this.basis.length)];
        }
        return name;
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }
}
