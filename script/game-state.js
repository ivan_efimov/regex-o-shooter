// Copyright (C) 2020 Ivan Efimov, https://gitlab.com/ivan_efimov

class Clock {
    constructor() {
        this.time = window.performance.now();
    }

    // in seconds
    getDelta() {
        let oldTime = this.time;
        this.time = window.performance.now();
        return 0.001 * (this.time - oldTime);
    }
}

class GameState {
    static UnitRadius = 1;
    static BaseHeightCoefficient = 0.1;

    constructor(gameRenderer) {
        this.UnitBaseSpeed = 0;
        this.EnemySpeedModifier = 0;
        this.AllySpeedModifier = 0;
        this.EnemyCountIncSpeed = 0;
        this.AllyCountIncSpeed = 0;

        this.health = 0;
        this.scores = 0;

        this.enemySpawnInterval = 0; // seconds
        this.allySpawnInterval = 0;
        this.enemySpawnDt = 0;
        this.allySpawnDt = 0;
        this.gameTotalTime = 0;

        this.maxEnemiesCount = 0;
        this.maxAlliesCount = 0;

        this.enemies = null;
        this.allies = null;

        this.clock = null;

        this.nameProvider = null;
        this.gameRenderer = gameRenderer;

        this.lastRegex = null;
    }

    reset(left, right, top, bottom, difficulty) {
        switch (difficulty) {
            case "extreme":
                this.UnitBaseSpeed = 3.25;
                this.EnemySpeedModifier = 0.1;
                this.AllySpeedModifier = 0.01;
                this.EnemyCountIncSpeed = 0.05;
                this.AllyCountIncSpeed = 0.06;
                this.health = 25;
                break;
            case "hard":
                this.UnitBaseSpeed = 3.25;
                this.EnemySpeedModifier = 0.05;
                this.AllySpeedModifier = 0.01;
                this.EnemyCountIncSpeed = 0.03;
                this.AllyCountIncSpeed = 0.04;
                this.health = 30;
                break;
            case "medium":
                this.UnitBaseSpeed = 2;
                this.EnemySpeedModifier = 0.04;
                this.AllySpeedModifier = 0.02;
                this.EnemyCountIncSpeed = 0.02;
                this.AllyCountIncSpeed = 0.02;
                this.health = 35;
                break;
            default:
                this.UnitBaseSpeed = 2;
                this.EnemySpeedModifier = 0.01;
                this.AllySpeedModifier = 0.01;
                this.EnemyCountIncSpeed = 0.01;
                this.AllyCountIncSpeed = 0.01;
                this.health = 40;
                break;
        }
        this.scores = 0;

        this.enemySpawnInterval = 1;
        this.allySpawnInterval = 1;
        this.enemySpawnDt = 0;
        this.allySpawnDt = 0;
        this.gameTotalTime = 0;

        this.maxEnemiesCount = 5;
        this.maxAlliesCount = 5;

        this.enemies = new UnitGroup(
            this.gameRenderer,
            new Line(
                left + GameState.UnitRadius,
                top - GameState.UnitRadius,
                right - GameState.UnitRadius,
                top - GameState.UnitRadius),
            function (pos) {
                return pos.y <= bottom + (top - bottom) * GameState.BaseHeightCoefficient + GameState.UnitRadius;
            },
            new UnitStats({x: 0, y: -this.UnitBaseSpeed}, GameState.UnitRadius),
            "enemy"
        );

        this.allies = new UnitGroup(
            this.gameRenderer,
            new Line(
                left + GameState.UnitRadius,
                bottom + (top - bottom) * GameState.BaseHeightCoefficient + GameState.UnitRadius,
                right - GameState.UnitRadius,
                bottom + (top - bottom) * GameState.BaseHeightCoefficient + GameState.UnitRadius),
            function (pos) {
                return pos.y >= top - GameState.UnitRadius;
            },
            new UnitStats({x: 0, y: this.UnitBaseSpeed}, GameState.UnitRadius),
            "ally"
        );

        this.clock = new Clock();
        this.nameProvider = new NameProvider();

        this.lastRegex = /^$/
    }

    update(regex, strikeRequired) {
        let dt = this.clock.getDelta();
        this.enemySpawnDt += dt;
        this.allySpawnDt += dt;
        this.gameTotalTime += dt;
        let wasSpawn = false;
        if (this.enemies.count() < this.maxEnemiesCount) {
            if (this.enemySpawnDt >= this.enemySpawnInterval) {
                while (this.enemies.count() < this.maxEnemiesCount) {
                    this.addUnit(this.enemies, new UnitStats(
                        {x: 0, y: -this.UnitBaseSpeed - this.EnemySpeedModifier * this.gameTotalTime},
                        GameState.UnitRadius));
                }
                this.enemySpawnDt %= this.enemySpawnInterval;
            }
            this.enemySpawnInterval = 1 + Math.random() * 2;
            wasSpawn = true;
        }
        if (this.allies.count() < this.maxAlliesCount) {
            if (this.allySpawnDt >= this.allySpawnInterval) {
                while (this.allies.count() < this.maxAlliesCount) {
                    this.addUnit(this.allies, new UnitStats(
                        {x: 0, y: this.UnitBaseSpeed + this.AllySpeedModifier * this.gameTotalTime},
                        GameState.UnitRadius));
                }
                this.allySpawnDt %= this.allySpawnInterval;
            }
            this.allySpawnInterval = 2 + Math.random() * 2;
            wasSpawn = true;
        }
        this.gameRenderer.clear();
        this.health -= Math.min(this.enemies.update(dt), this.health);
        this.allies.update(dt);
        if (regex !== this.lastRegex || wasSpawn) {
            this.check(regex);
            this.lastRegex = regex;
        }
        if (strikeRequired) {
            this.strike();
        }
        this.gameRenderer.drawCrashes();
        this.gameRenderer.drawStrikes();
        this.maxEnemiesCount = 5 + Math.floor(this.gameTotalTime * this.EnemyCountIncSpeed);
        this.maxAlliesCount = 5 + Math.floor(this.gameTotalTime * this.AllyCountIncSpeed);
    }

    check(re) {
        this.enemies.check(re);
        this.allies.check(re);
    }

    strike() {
        let enemiesKilled = this.enemies.strike();
        this.scores += enemiesKilled * enemiesKilled;
        let alliesKilled = this.allies.strike();
        this.health -= Math.min(alliesKilled * alliesKilled, this.health);
    }

    addUnit(group, stats = null) {
        let name;
        do {
            name = this.nameProvider.getName();
        } while (this.enemies.nameIsPresent(name) || this.allies.nameIsPresent(name))
        group.spawn(name, stats);
    }
}